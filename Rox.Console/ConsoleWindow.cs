﻿using System;

namespace Rox.Console
{
    public class ConsoleWindow : Form
    {
        protected ConsoleWindow(int left, int top, int width, int height) : base(left, top, width, height)
        {
        }

        protected string Title { private get; set; } = string.Empty;

        protected void AddControl(Control control)
        {
            control.Parent = this;
            Controls.Add(control);
        }

        public void Render()
        {
            Render(0, 1);
        }

        protected override void RenderControlContent(int left, int top)
        {
            System.Console.CursorVisible = false;
            System.Console.BackgroundColor = ConsoleColor.Blue;
            for (var j = 0; j < Height; j++)
            {
                System.Console.SetCursorPosition(left, top + j);
                System.Console.Write(" ".PadRight(Width));
            }

            System.Console.BackgroundColor = ConsoleColor.Cyan;
            System.Console.ForegroundColor = ConsoleColor.White;
            System.Console.SetCursorPosition(left, top);
            System.Console.Write(Title.PadRight(Width));
            System.Console.BackgroundColor = ConsoleColor.Blue;
        }
    }
}