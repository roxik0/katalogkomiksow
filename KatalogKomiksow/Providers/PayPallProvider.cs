﻿namespace KatalogKomiksow.Providers
{
    internal class PayPallProvider
    {
        public string PayByPayPallRequest(decimal amount)
        {
            return $"PayPal:{amount}";
        }
    }
}