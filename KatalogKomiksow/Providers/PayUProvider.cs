﻿namespace KatalogKomiksow.Providers
{
    internal class PayUProvider
    {
        public string PayByPayURequest(decimal amount, double discount)
        {
            return $"PayU:{amount}|Discount:{discount}";
        }
    }
}