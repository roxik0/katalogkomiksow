﻿using System;

namespace KatalogKomiksow
{
    public class ComicBook : ISellable, ILendable, IRestriction
    {
        public Person Author { get; set; }
        public User Owner { get; set; }
        public decimal Price { get; set; }

        public bool IsLimitedForPerson(Person person)
        {
            //TODO: Using strategy add limitation depend on Age :)
            throw new NotImplementedException("Make limitation by age here");
        }
    }
}