﻿using System;
using System.Collections.Generic;

namespace KatalogKomiksow
{
    public class ComicBookStore : IStore
    {
        public List<ComicBookOffer> Catalog { get; set; }

        public void AddToCatalog(ComicBook comicBook)
        {
            throw new NotImplementedException();
        }

        public void Sell(ComicBook comicBook)
        {
            throw new NotImplementedException();
        }

        public string Buy(ComicBook comicBook, PaymentMethod paymentMethod)
        {
            var paymentProvider = PaymentProviderFactory.GetPaymentProvider(paymentMethod);
            return paymentProvider.Pay(comicBook.Price);
        }
    }

    public class PaymentProviderFactory
    {
        public static PaymentProvider GetPaymentProvider(PaymentMethod paymentMethod)
        {
            //TODO Tu powinien być switch który wybierze odpowiedni Adapter :)
            return new PaymentProvider();
        }
    }

    public class PaymentProvider
    {
        public string Pay(decimal amount)
        {
            return $"Default Payment Provider: Success{amount} ";
        }
    }
}